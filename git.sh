#!/bin/bash
#echo "Скрипт для работы с git. Введите путь:"
#read path;
path='/tmp/test/hello';
echo "Выберите пунк меню:";
echo "1 - создать локальный репо";
echo "2 - удалить локальный репо";
echo "3 - добавить файл в репо";
echo "4 - удалить файл из репо";
echo "5 - проверить текущее состояние репо";
echo "6 - cделать commit";
echo "7 - добавить удалённый репо";
echo "8 - сделать push";
echo "9 - клонирование репо с удаленной машины";
echo "10 - cкачать последнюю версию кода с удаленной машины";
echo "11 - создать ветку";
echo "12 - удалить тупиковую ветку";
echo "13 - показать все ветки";
echo "14 - показать все коммиты";
echo "15 - отмена последнего коммита"
read item;
if (($item==1))
then
	cd $path;
	git init;
	echo "Репозиторий создан!";
fi
if (($item==2))
then
	cd $path;
	rm -r .git;
	echo "Репозиторий удален!";
fi
if (($item==3))
then
	cd $path;
	echo "Введите название файла, который хотите добавить в репо:";
	read file_name;
	git add $file_name;
	echo "файл успешно добавлен!";
fi
if (($item==4))
then
	cd $path;
	echo "Введите название файла, который хотите удалить:";
	read file_name;
	git rm $file_name;
	echo "файл успешно удален!";
fi
if (($item==5))
then
	cd $path;
	git status;
fi
if (($item==6))
then
	cd $path;
	echo "Введите комментарий";
	read comment;
	git commit -m $comment;
fi
if (($item==7))
then
	cd $path;
	echo "Введите ссылку на удаленный репозиторий:";
	read site;
	echo "Введите название удаленного репо:"
	read origin;
	git remote add $origin $site;
fi
if (($item==8))
then
	cd $path;
	echo "Введите название удалённого репо:";
	read name_repo;
	echo "Введите название ветки:"
	read name_branch;
	git push -u $name_repo $name_branch;
fi
if (($item==9))
then
	cd $path;
	echo "Введите ссылку на удаленный репозиторий:";
	read site;
	git clone $site;
fi
if (($item==10))
then
	cd $path;
	echo "Введите название удалённого репо:";
	read name_repo;
	echo "Введите название ветки:"
	read name_branch;
	git pull $name_repo $name_branch;
fi
if (($item==11))
then
	cd $path;
	echo "Введите название новой ветки:";
	read name_branch;
	git branch $name_branch;
fi
if (($item==12))
then
	cd $path;
	echo "Введите название ветки, которую хотите удалить:";
	read name_branch;
	git branch -D $name_branch;
fi
if (($item==13))
then
	cd $path;
	git branch;
fi
if (($item==14))
then
	cd $path;
	git log;
fi
if (($item==15))
then
	cd $path;
	git reset --hard HEAD~1;
fi